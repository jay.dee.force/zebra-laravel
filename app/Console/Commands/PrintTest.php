<?php

namespace App\Console\Commands;

use Zebra\Client;
use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;
use Illuminate\Console\Command;

class PrintTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:test {printer=10.0.0.50}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends image to a zebra network connected printer.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $printerIp = $this->argument('printer');

        $decoder = GdDecoder::fromPath(public_path('ZebraGray.png'));

        $image = new Image($decoder);

        $zpl = new Builder();
        //The following example will print a label with an image positioned 50 dots from the top left.
        //https://github.com/robgridley/zebra
        $zpl->fo(50, 50)->gf($image)->fs();

        $client = new Client($printerIp);
        $client->send($zpl);

        $this->info('The command was successful!');

    }
}
