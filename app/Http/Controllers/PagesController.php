<?php

namespace App\Http\Controllers;

use Zebra\Client;
use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function sample(){
        return view('sample');
    }

    public function printTest(){

        $decoder = GdDecoder::fromPath(public_path('ZebraGray.png'));

        $image = new Image($decoder);

        $zpl = new Builder();
        $zpl->fo(50, 50)->gf($image)->fs();

        $client = new Client('10.0.0.50');
        $client->send($zpl);

        return response()->json([
            'sucess' => true
        ]);
      
    }

}
