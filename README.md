# INSTALLATION

Download the code and run the following commands:

`cp .env.example .env`

`composer install`

`php artisan key:generate`

`php artisan serve`

This will start a web server in port http://localhost:8000

Next install the appropriate native application for your OS (Mac, Windows, Android) at: https://www.zebra.com/us/en/support-downloads/printer-software/by-request-software.html

# USAGE

## Browser based

Visit `/sample` to run a Print Test Page built with: https://www.zebra.com/us/en/forms/browser-print-request-javascript.html

Visit `/documentation.html` to see the Browser Print documentation

## Sockets/Server based

Run `php artisan print:test {ip of the zebra printer}` to send a test print to a specific zebra printer.
